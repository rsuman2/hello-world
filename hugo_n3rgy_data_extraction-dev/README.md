# Hugo N3rgy Data Migration

Extracting data from N3rgy API and uploading to the database.


## Local Setup

Create a virtual environment

```commandline
>>> python3 -m venv venv
```

Activate the virtual environment

```commandline
>>> source venv/bin/activate
```

Install all the requirements using `requirements.txt`

```commandline
>>> pip install -r requirements.txt
```