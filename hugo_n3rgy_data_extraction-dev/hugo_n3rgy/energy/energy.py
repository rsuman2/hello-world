import requests

from hugo_n3rgy.constant import HEADERS

class Energy():
    def __init__(self, mpxn:str, utility:str, start_date_and_time:str , end_date_and_time:str, granularity:str)-> None:
        self.mpxn  = mpxn 
        self.utility = utility
        self.start_date_and_time = start_date_and_time
        self.end_date_and_time = end_date_and_time
        self.granularity= granularity
    
    def get_energy_url_details(self):
        url = f"https://api.data.n3rgy.com/{self.mpxn}/{self.utility}/consumption/1?start={self.start_date_and_time}&end={self.end_date_and_time}&granularity={self.granularity}"
        api_response = requests.get(url, headers=HEADERS)
        return api_response

    