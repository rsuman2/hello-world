from json import loads
from sqlalchemy import create_engine

from hugo_n3rgy.energy.energy import Energy

class Meter:
    def __init__(self, mpxn:str, utility:str, start_date_and_time:str , end_date_and_time:str, granularity:str)-> None:
        self.mpxn  = mpxn 
        self.utility = utility
        self.start_date_and_time = start_date_and_time
        self.end_date_and_time = end_date_and_time
        self.granularity= granularity
        self.n3rgy =  Energy(self.mpxn,self.utility,self.start_date_and_time,self.end_date_and_time,self.granularity)

    def get_consent_status(self)->str:
         # DEFINE THE ENGINE (CONNECTION OBJECT)
        engine = create_engine(
	    "mysql+pymysql://hugoapi:urinci60@hugo-test-db-1.nuanced.co.uk:3306/hugo_test")

        try:
           query = engine.execute(f"SELECT consent_status FROM hugo_test.meters WHERE mprn_mpan = {self.mpxn}")
           save_in_dict = [dict(row) for row in query]
           consent_status = save_in_dict[0]['consent_status']           
           return consent_status           
        except Exception as ex:
           print("Connection could not be made due to the following error: \n", ex)
        
    def get_meter_data(self):
        api_responce  = Energy.get_energy_url_details(self)
        json_data = api_responce.content
        meter_data = dict(loads(json_data))         
        return meter_data  

    def meter_data_display(self): 
        dict_data = self.get_meter_data()
        for i in range(len(dict_data['values'])):
            #print(dict_data['values'][i])
            print(round(dict_data['values'][i]['value'],2))  

    def upload_in_db(self):
        # DEFINE THE ENGINE (CONNECTION OBJECT) will update below URL
        engine = create_engine(
	    "mysql+pymysql://root:root@127.0.0.1:13306/Hugon3rgy")

        dict_data = self.get_meter_data()
        for i in range(len(dict_data['values'])):
            id  = engine.execute("INSERT INTO Hugon3rgy.meterdata(Id,Timestamp,Value) VALUES("+str(i)+",'"
                  +str(dict_data['values'][i]['timestamp'])+"','"+str(dict_data['values'][i]['value'])+"')")            


