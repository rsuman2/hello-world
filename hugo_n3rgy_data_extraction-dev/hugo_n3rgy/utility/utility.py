from sqlalchemy import create_engine

from hugo_n3rgy.meter.meter import Meter

from datetime import datetime 
from datetime import timedelta
from decimal import Decimal

CALORIFIC_VALUE = 39.5
CORRECTION_FACTOR = 1.02264
CONVERSION_FACTOR = 3.6

granularity = "halfhour"

engine = create_engine(
	    "mysql+pymysql://hugoapi:urinci60@hugo-test-db-1.nuanced.co.uk:3306/hugo_test")
    
class Tariff_Calculation:
 def __init__(self , mpxn:str)-> None:
          self.mpxn = mpxn

 def get_meter_value_list(self, mpxn ,utility, start_date_and_time, end_date_and_time, granularity):
     self.utility = utility
     self.start_date_and_time =start_date_and_time
     self.end_date_and_time = end_date_and_time
     self.granularity = granularity

     is_consent_status = Meter.get_consent_status(self)
     
     # Check consent_status 
     if is_consent_status=="SUCCESS":
          energy_result = Meter(mpxn, utility, start_date_and_time, end_date_and_time, granularity)
     
          meter_data =  Meter.get_meter_data(energy_result)
          
          # check values filed available or not
          if meter_data.get('values') != None: 
               list_of_values = []
               for i in range(len(meter_data['values'])):
                    decimal_value = Decimal(meter_data['values'][i]['value'])
                    round_of_value_by_2 = round(decimal_value,2) 
                    list_of_values.append(float(round_of_value_by_2))
               return list_of_values 
          # check error           
          elif meter_data['errors'][0]['code']==400:
               message = meter_data['errors'][0]['message']
               print("getting Error " ,message)
     else:
          print("consent status not found")          

 def get_electricity_list(self):
     meters_row_dict = self.get_meter_table_data_by_mpxn()

     elec_last_consumption_date = meters_row_dict[0]["elec_last_consumption_date"]
     if elec_last_consumption_date == None:
        cureent_date = datetime.now().date()
        before_90_days_date = datetime.today() - timedelta(days=90)
        before_90_days_date = datetime.strftime(cureent_date,"%Y%m%d0000")

        start_date = before_90_days_date
        start_date_time = Tariff_Calculation.get_datetime_after_30_minute(start_date)
        end_date_time = datetime.now().date().strftime("%Y%m%d0000")

        list_of_electricity_value = self.get_meter_value_list(self.mpxn,"electricity",start_date_time,end_date_time,granularity)
     else:
       # start_date = datetime.strftime(elec_last_consumption_date,"%Y%m%d0000")   
        # will be remove below two line : just for testing
        elec_last_consumption_date = datetime.today() - timedelta(days=1)
        start_date = datetime.strftime(elec_last_consumption_date,"%Y%m%d0000")
        start_date_time = Tariff_Calculation.get_datetime_after_30_minute(start_date)
        end_date_time =  datetime.now().date().strftime("%Y%m%d0000")
        
        list_of_electricity_value = self.get_meter_value_list(self.mpxn,"electricity",start_date_time,end_date_time,granularity)
        # print("electricity value :",list_of_electricity_value)
     return list_of_electricity_value

 def get_gas_list(self):
     meters_row_dict = self.get_meter_table_data_by_mpxn()

     gas_last_consumption_date = meters_row_dict[0]["gas_last_consumption_date"]
     if gas_last_consumption_date == None:
        cureent_date = datetime.now().date()
        before_90_days_date = datetime.today() - timedelta(days=90)
        before_90_days_date = datetime.strftime(cureent_date,"%Y%m%d0000")

        start_date = before_90_days_date
        start_date_time = Tariff_Calculation.get_datetime_after_30_minute(start_date)
        end_date_time = datetime.now().date().strftime("%Y%m%d0000")

        list_of_gas_value = self.get_meter_value_list(self.mpxn,"gas",start_date_time,end_date_time,granularity)
     else:
       # start_date = datetime.strftime(gas_last_consumption_date,"%Y%m%d0000")   
        # will be remove below two line : just for testing
        gas_last_consumption_date = datetime.today() - timedelta(days=1)
        start_date = datetime.strftime(gas_last_consumption_date,"%Y%m%d0000")
        start_date_time = Tariff_Calculation.get_datetime_after_30_minute(start_date)
        end_date_time =  datetime.now().date().strftime("%Y%m%d0000")
        list_of_gas_value = self.get_meter_value_list(self.mpxn,"gas",start_date_time,end_date_time,granularity)
        print("gas value :",list_of_gas_value)
     return list_of_gas_value   

 def get_datetime_after_30_minute(date_and_time:str):         
     date_ = date_and_time[:8]
     time_ = date_and_time[8:]
     date_in_datetime =  datetime.strptime(date_,'%Y%m%d').date()
     time_in_datetime =  datetime.strptime(time_,'%H%M').time()
     datetime_join = datetime.combine(date_in_datetime,time_in_datetime)

     datetime_after_30_minute = str(datetime_join + timedelta(minutes=30))
     datetime_after_30_minute = ''.join(letter for letter in datetime_after_30_minute if letter.isalnum())
          
     datetime_after_30_minute = datetime_after_30_minute[:len(datetime_after_30_minute)-2] # for the formate yyyymmddhhmm
     #print(" After 30 min date :",datetime_after_30_minute)
     return datetime_after_30_minute

 def get_meter_table_data_by_mpxn(self):
     query  = engine.execute(f"SELECT * FROM hugo_test.meters WHERE mprn_mpan = {self.mpxn}")
     meters_row_dict = [dict(row) for row in query] 
     return meters_row_dict
# ******* Above code will get the meter values according to date and time for specific
# ******* energy value (electricity and gas)

 def electricity_tarrif_calculation(self):
     manual_tariff_log_dict = self.get_manual_log_by_utility_id("1") #electricity
     # Check if dictionary is empty
     is_dict_empty = not bool(manual_tariff_log_dict)
     
    # code for default setting 

     if is_dict_empty==True:
        # for user in default setting:
        default_rates = Tariff_Calculation.get_default_rates_log("1")
        
        day_unit_rate_in_pence = default_rates[0]['day_unit_rate_in_pence']
        night_unit_rate_in_pence = default_rates[0]['night_unit_rate_in_pence']

        electricity_value_list = self.get_electricity_list()
        # print("elctricity_value_list................................................")
        # for i in range(0,48):
        #   print(i,electricity_value_list[i])
        start_night_time = datetime.strptime("00:30","%H:%M").time()
        end_night_time = datetime.strptime("07:00","%H:%M").time()

        hh_slots = Tariff_Calculation.convert_time_to_hh_slot(start_night_time,end_night_time)
        
        night_start_slot= hh_slots[0]
        night_end_slot = hh_slots[1]

        print("Night slot",night_start_slot)
        print("day slot",night_end_slot)

        hh_slot_list = Tariff_Calculation.get_hh_slots_list(night_start_slot , night_end_slot)

        print("night slot list",hh_slot_list[0])
        print("day slot list",hh_slot_list[1])

        night_consumption_cost_dict = {}
        day_consumption_cost_dict = {}

        for i in range(0,len(hh_slot_list[0])):

          night_consumption_cost_dict[i] = round(electricity_value_list[i]*night_unit_rate_in_pence/100,2)

        print("night consumption_cost_dict",night_consumption_cost_dict)  

        for i in range(hh_slot_list[1][0]-1 , hh_slot_list[1][len(hh_slot_list[1])-1]):
          day_consumption_cost_dict[i] = round(electricity_value_list[i]*day_unit_rate_in_pence/100,2)
        print("day_consumption_cost_dict",day_consumption_cost_dict)  
    
        print("upload_section starts for electricity")
        meter_id =   self.get_meter_ID()

        elec_last_consumption_date = datetime.today() - timedelta(days=1)
        start_date = datetime.strftime(elec_last_consumption_date,"%Y-%m-%d 00:00:00")
        print("@",start_date)

        electricity_value_list = self.get_electricity_list()
        electricity_value_list = self.get_electricity_list()
        Tariff_Calculation.data_upload_to_meter_consumption_tarrif(meter_id,1,start_date,night_consumption_cost_dict,day_consumption_cost_dict,electricity_value_list,
        0.00,day_unit_rate_in_pence,night_unit_rate_in_pence,day_unit_rate_in_pence)       
      
     else:
      # for user in  manual setting with single rate:
        electricity_value_list = self.get_electricity_list()
       
        day_unit_rate = manual_tariff_log_dict[0]['day_unit_rate']
        print("day_unit_rate",day_unit_rate)
        night_unit_rate = manual_tariff_log_dict[0]['night_unit_rate']
        is_single_rate = manual_tariff_log_dict[0]['is_single_rate']

        if manual_tariff_log_dict[0]['is_single_rate'] != 0: 
            manual_single_day_unit_rate = manual_tariff_log_dict[0]['day_unit_rate']
            day_unit_rate_in_pence = manual_tariff_log_dict[0]['day_unit_rate']
            night_unit_rate_in_pence =manual_tariff_log_dict[0]['night_unit_rate']

            electricity_value_list = self.get_electricity_list()
        
            start_night_time = datetime.strptime("00:30","%H:%M").time()
            end_night_time = datetime.strptime("07:00","%H:%M").time()

            hh_slots = Tariff_Calculation.convert_time_to_hh_slot(start_night_time,end_night_time)
        
            night_start_slot= hh_slots[0]
            night_end_slot = hh_slots[1]

            print("Night slot",night_start_slot)
            print("day slot",night_end_slot)

            hh_slot_list = Tariff_Calculation.get_hh_slots_list(night_start_slot , night_end_slot)

            print("night slot list",hh_slot_list[0])
            print("day slot list",hh_slot_list[1])

            night_consumption_cost_dict = {}
            day_consumption_cost_dict = {}
            print("........................")
            print(electricity_value_list)
            print(night_unit_rate)
            print("........................")

            for i in range(0,len(hh_slot_list[0])):
              night_consumption_cost_dict[i] = round(electricity_value_list[i]*night_unit_rate/100,2)

            print("night consumption_cost_dict",night_consumption_cost_dict)  

            for i in range(hh_slot_list[1][0]-1 , hh_slot_list[1][len(hh_slot_list[1])-1]):
              day_consumption_cost_dict[i] = round(electricity_value_list[i]*day_unit_rate/100,2)
            print("day_consumption_cost_dict",day_consumption_cost_dict)  
            
            print("upload_section starts for electricity")
            meter_id =   self.get_meter_ID()

            elec_last_consumption_date = datetime.today() - timedelta(days=1)
            start_date = datetime.strftime(elec_last_consumption_date,"%Y-%m-%d 00:00:00")
            print("@",start_date)

            electricity_value_list = self.get_electricity_list()
            electricity_value_list = self.get_electricity_list()
            Tariff_Calculation.data_upload_to_meter_consumption_tarrif(meter_id,1,start_date,night_consumption_cost_dict,day_consumption_cost_dict,electricity_value_list,
        0.00,day_unit_rate_in_pence,night_unit_rate_in_pence,day_unit_rate_in_pence)  

    # code for manual setting with day/night unit rate
        else:
          # for user in manual setting with day unit rate:
          print("manual day night calculation")
          electricity_value_list = self.get_electricity_list()
          day_unit_rate = manual_tariff_log_dict[0]['day_unit_rate']
          night_unit_rate = manual_tariff_log_dict[0]['night_unit_rate']
          print("stoppping")
          start_night_time = str(manual_tariff_log_dict[0]['night_start_time'])
          end_night_time  = str (manual_tariff_log_dict[0]['night_end_time'])


          start_night_time = datetime.strptime(start_night_time,"%H:%M:%S").time()
          end_night_time = datetime.strptime(end_night_time,"%H:%M:%S").time()

          print("night_end_time",type(end_night_time))
          print("night_start_time",start_night_time)
          
          hh_slots = Tariff_Calculation.convert_time_to_hh_slot(start_night_time,end_night_time)
        
          night_start_slot= hh_slots[0]
          night_end_slot = hh_slots[1]

          print("Night slot",night_start_slot)
          print("day slot",night_end_slot)

          hh_slot_list = Tariff_Calculation.get_hh_slots_list(night_start_slot , night_end_slot)

          print("night slot list",hh_slot_list[0])
          print("day slot list",hh_slot_list[1])

          night_consumption_cost_dict = {}
          day_consumption_cost_dict = {}
          
          print("night_unit_rate.................",night_unit_rate)
          for i in range(0,len(hh_slot_list[0])):
              night_consumption_cost_dict[i] = round(electricity_value_list[i]*night_unit_rate/100,2)

          print("night consumption_cost_dict",night_consumption_cost_dict)  

          for i in range(hh_slot_list[1][0]-1 , hh_slot_list[1][len(hh_slot_list[1])-1]):
              day_consumption_cost_dict[i] = round(electricity_value_list[i]*day_unit_rate/100,2)
          print("day_consumption_cost_dict",day_consumption_cost_dict)

          print("upload_section starts for electricity")
          meter_id =   self.get_meter_ID()

          elec_last_consumption_date = datetime.today() - timedelta(days=1)
          start_date = datetime.strftime(elec_last_consumption_date,"%Y-%m-%d 00:00:00")
          print("@",start_date)

          electricity_value_list = self.get_electricity_list()
          Tariff_Calculation.data_upload_to_meter_consumption_tarrif(meter_id,1,start_date,night_consumption_cost_dict,day_consumption_cost_dict,electricity_value_list,
        0.00,day_unit_rate,night_unit_rate,day_unit_rate)  


 

        # print("manual_tariff_log_dict not available")
 
 # gas kwh conversion is remaning : will do later :::::
 def gas_tarrif_calculation(self):
     manual_tariff_log_dict = self.get_manual_log_by_utility_id("2") #gas
     # Check if dictionary is empty
     is_dict_empty = not bool(manual_tariff_log_dict)

     if is_dict_empty==True:
        # user in default setting
        default_rates = Tariff_Calculation.get_default_rates_log("2")
        
        day_unit_rate_in_pence = default_rates[0]['day_unit_rate_in_pence']
        night_unit_rate_in_pence = default_rates[0]['night_unit_rate_in_pence']

        gas_value_list = self.get_gas_list()

        gas_value_kwh_list = []  
        for i in range(len(gas_value_list)):
          gas_kwh_conversion  = gas_value_list[i] * CALORIFIC_VALUE * CORRECTION_FACTOR / CONVERSION_FACTOR               
          gas_value_kwh_list.append(round(gas_kwh_conversion,2))
        
        print("gas kwh value",gas_value_kwh_list)
        
        start_night_time = datetime.strptime("00:30","%H:%M").time()
        end_night_time = datetime.strptime("07:00","%H:%M").time()

        hh_slots = Tariff_Calculation.convert_time_to_hh_slot(start_night_time,end_night_time)
        
        night_start_slot= hh_slots[0]
        night_end_slot = hh_slots[1]

        print("Night slot",night_start_slot)
        print("day slot",night_end_slot)

        hh_slot_list = Tariff_Calculation.get_hh_slots_list(night_start_slot , night_end_slot)

        print("night slot list",hh_slot_list[0])
        print("day slot list",hh_slot_list[1])

        night_consumption_cost_dict = {}
        day_consumption_cost_dict = {}

        for i in range(0,len(hh_slot_list[0])):
          night_consumption_cost_dict[i] = round(gas_value_kwh_list[i]*night_unit_rate_in_pence/100,2)

        print("night consumption_cost_dict",night_consumption_cost_dict)  

        for i in range(hh_slot_list[1][0]-1 , hh_slot_list[1][len(hh_slot_list[1])-1]):
          day_consumption_cost_dict[i] = round(gas_value_kwh_list[i]*day_unit_rate_in_pence/100,2)
        print("day_consumption_cost_dict",day_consumption_cost_dict)  

        ## upload data on server Or db
        print("upload_section starts for Gas")
        meter_id =   self.get_meter_ID()

        elec_last_consumption_date = datetime.today() - timedelta(days=1)
        start_date = datetime.strftime(elec_last_consumption_date,"%Y-%m-%d 00:00:00")
        print("@",start_date)

        # electricity_value_list = self.get_electricity_list()
        # electricity_value_list = self.get_electricity_list()
        Tariff_Calculation.data_upload_to_meter_consumption_tarrif(meter_id,2,start_date,night_consumption_cost_dict,day_consumption_cost_dict,gas_value_kwh_list,
        gas_value_list,day_unit_rate_in_pence,night_unit_rate_in_pence,day_unit_rate_in_pence)       
         
     else: # user in manual setting 
      gas_value_list = self.get_gas_list()
       
      day_unit_rate = manual_tariff_log_dict[0]['day_unit_rate']
      print("day_unit_rate",day_unit_rate)
      night_unit_rate = manual_tariff_log_dict[0]['night_unit_rate']
      is_single_rate = manual_tariff_log_dict[0]['is_single_rate']

      if manual_tariff_log_dict[0]['is_single_rate'] != 0: 
            manual_single_day_unit_rate = manual_tariff_log_dict[0]['day_unit_rate']
            day_unit_rate_in_pence = manual_tariff_log_dict[0]['day_unit_rate']
            night_unit_rate_in_pence =manual_tariff_log_dict[0]['night_unit_rate']

            gas_value_list = self.get_gas_list()

            gas_value_kwh_list = []  
            for i in range(len(gas_value_list)):
             gas_kwh_conversion  = gas_value_list[i] * CALORIFIC_VALUE * CORRECTION_FACTOR / CONVERSION_FACTOR               
             gas_value_kwh_list.append(round(gas_kwh_conversion,2))
        
            print("gas kwh value",gas_value_kwh_list)
        
            start_night_time = datetime.strptime("00:30","%H:%M").time()
            end_night_time = datetime.strptime("07:00","%H:%M").time()

            hh_slots = Tariff_Calculation.convert_time_to_hh_slot(start_night_time,end_night_time)
        
            night_start_slot= hh_slots[0]
            night_end_slot = hh_slots[1]

            print("Night slot",night_start_slot)
            print("day slot",night_end_slot)

            hh_slot_list = Tariff_Calculation.get_hh_slots_list(night_start_slot , night_end_slot)

            print("night slot list",hh_slot_list[0])
            print("day slot list",hh_slot_list[1])

            night_consumption_cost_dict = {}
            day_consumption_cost_dict = {}

            for i in range(0,len(hh_slot_list[0])):
              night_consumption_cost_dict[i] = round(gas_value_kwh_list[i]*night_unit_rate/100,2)

            print("night consumption_cost_dict",night_consumption_cost_dict)  

            for i in range(hh_slot_list[1][0]-1 , hh_slot_list[1][len(hh_slot_list[1])-1]):
              day_consumption_cost_dict[i] = round(gas_value_kwh_list[i]*day_unit_rate/100,2)
            print("day_consumption_cost_dict",day_consumption_cost_dict)  
            
            print("upload_section starts for Gas")
            meter_id =   self.get_meter_ID()

            elec_last_consumption_date = datetime.today() - timedelta(days=1)
            start_date = datetime.strftime(elec_last_consumption_date,"%Y-%m-%d 00:00:00")
            print("@",start_date)

            electricity_value_list = self.get_electricity_list()
            Tariff_Calculation.data_upload_to_meter_consumption_tarrif(meter_id,2,start_date,night_consumption_cost_dict,day_consumption_cost_dict,gas_value_kwh_list,
        gas_value_list,day_unit_rate_in_pence,night_unit_rate_in_pence,day_unit_rate_in_pence)       
    


 def convert_time_to_hh_slot(start_time:datetime, end_time:datetime):       
     midnight_delta = timedelta(hours=0)
     thirty_min_delta = timedelta(minutes=30)
     night_start_delta = timedelta(hours=start_time.hour, minutes=start_time.minute)
     night_end_delta = timedelta(hours=end_time.hour, minutes=end_time.minute)
     start_hh_slot = (night_start_delta - midnight_delta).total_seconds() / thirty_min_delta.total_seconds()
     end_hh_slot = (night_end_delta - midnight_delta).total_seconds() / thirty_min_delta.total_seconds()
     start_hh_slot = start_hh_slot+1 if start_hh_slot == 0 else start_hh_slot
     print(start_hh_slot)
     print(end_hh_slot)
     return int(start_hh_slot), int(end_hh_slot)
     
     
 def get_hh_slots_list(night_start_slot=1, night_end_slot=14):
     whole_day_slots = list(range(1,49))

     if night_end_slot > night_start_slot:
        night_hh_slots = list(range(night_start_slot, night_end_slot+1))
     else:
        night_hh_slots = [i for i in range(night_start_slot+1, 49)]
        night_hh_slots += [i for i in range(1, night_end_slot+1)]
     day_hh_slots = list(set(whole_day_slots) - set(night_hh_slots))
     return night_hh_slots, day_hh_slots    

 def get_manual_log_by_utility_id(self,utility_key)->dict:
     meter_id = self.get_meter_ID() 
     query = engine.execute(f"SELECT * FROM hugo_test.manual_tariff_log WHERE meter_id = {meter_id} AND utility_type_id = {utility_key}")                    
     manual_tariff_log_dict = [dict(row) for row in query]         
     return manual_tariff_log_dict
 
 def get_meter_ID(self)->int:
     query = engine.execute(f"SELECT meter_id FROM hugo_test.user_meters WHERE mprn_mpan = {self.mpxn}")                    
     save_in_dict = [dict(row) for row in query]
     meter_id = save_in_dict[0]['meter_id']          
     return meter_id
     
     

 def get_default_rates_log(utility):
     cureent_date = datetime.strftime(datetime.now(), "%Y-%m-%d") 
     query = engine.execute(f"SELECT * FROM hugo_test.default_rates_log WHERE utility_type_id = {utility}  AND '{cureent_date}' >= effective_from AND '{cureent_date}'<= date (effective_till)")                    
     save_in_dict = [dict(row) for row in query]                  
     return save_in_dict 
  

  ### **********************  above code for calculation   ********************* 
 
 def data_upload_to_meter_consumption_tarrif(meter_id,utility_type,reading_date,night_con_cost,day_con_cost,consumption_unit,gas_m3_value,tariff,default_rate_night,default_rate_day):   
  engine = create_engine("mysql+pymysql://hugoapi:urinci60@hugo-test-db-1.nuanced.co.uk:3306/hugo_test")
  
  meter_id_ = meter_id
  utility_type_id = utility_type
  
  
  reading_date_ = reading_date
  
  night_con_cost_ = night_con_cost
  day_con_cost_ =day_con_cost
  # gas_value_list = self.get_gas_list()
  # gas_value_list = []
  # gas_value_list = self.get_gas_list()
  
  consumption_unit_ = consumption_unit
  if utility_type_id ==1:
   gas_m3_value_ = 0.00
  else:  
    # for k in range(len(gas_value_list)):
    #   gas_m3_value_  = gas_value_list[k]
    gas_m3_value_ = gas_m3_value

  tariff_ = tariff/100 
  default_rate_night_ = default_rate_night/100
  default_rate_day_ = default_rate_day/100

  if utility_type_id ==1:
   for k in night_con_cost_.keys():
     id = engine.execute(f"INSERT INTO hugo_test.rakhi_meter_consumption_tariff_1(meter_id,utility_type_id,reading_date,hh_slot,consumption_units,consumption_pounds_total_cost,gas_m3_value,tariff,default_rate_night,default_rate_day) VALUES({meter_id_},{utility_type_id},'{reading_date_}',{k+1},{consumption_unit_[k]},{night_con_cost_[k]},{gas_m3_value_},{tariff_},{default_rate_night_},{default_rate_day_})")
  
   for k in day_con_cost_.keys():
     id = engine.execute(f"INSERT INTO hugo_test.rakhi_meter_consumption_tariff_1(meter_id,utility_type_id,reading_date,hh_slot,consumption_units,consumption_pounds_total_cost,gas_m3_value,tariff,default_rate_night,default_rate_day) VALUES({meter_id_},{utility_type_id},'{reading_date_}',{k+1},{consumption_unit_[k]},{day_con_cost_[k]},{gas_m3_value_},{tariff_},{default_rate_night_},{default_rate_day_})")
  elif utility_type_id ==2:
    print("utility_type_id",utility_type_id , gas_m3_value_)
    for k in night_con_cost_.keys():
     id = engine.execute(f"INSERT INTO hugo_test.rakhi_meter_consumption_tariff_1(meter_id,utility_type_id,reading_date,hh_slot,consumption_units,consumption_pounds_total_cost,gas_m3_value,tariff,default_rate_night,default_rate_day) VALUES({meter_id_},{utility_type_id},'{reading_date_}',{k+1},{consumption_unit_[k]},{night_con_cost_[k]},{gas_m3_value_[k]},{tariff_},{default_rate_night_},{default_rate_day_})")
  
    for k in day_con_cost_.keys():
     id = engine.execute(f"INSERT INTO hugo_test.rakhi_meter_consumption_tariff_1(meter_id,utility_type_id,reading_date,hh_slot,consumption_units,consumption_pounds_total_cost,gas_m3_value,tariff,default_rate_night,default_rate_day) VALUES({meter_id_},{utility_type_id},'{reading_date_}',{k+1},{consumption_unit_[k]},{day_con_cost_[k]},{gas_m3_value_[k]},{tariff_},{default_rate_night_},{default_rate_day_})")
 
  print("Uploading Done")
